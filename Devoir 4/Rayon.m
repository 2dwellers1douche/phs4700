classdef Rayon < handle
    %RAYON Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(GetAccess = 'public', SetAccess = 'public')
        positions;
        angles;
        normale;
        plan;
        isGood = false;
        positionFinale;
    end
    
    methods
        
        %constructeur
        function obj = Rayon(posObs, point, angle, plan)
            obj.positions = cat(1, transpose(posObs), point);
            obj.angles = cat(1, 0, angle);
            obj.plan = plan;
        end
        
        function go(obj, indiceRefraction0, indiceRefractiont, ...
                plansBoites)
            compteur = 0;
            mustGoOn = true;
            
            i = obj.plan.normale;
            
            [height width] = size(obj.positions);
            
            u_i = transpose(obj.positions(height, :)...
                - obj.positions(height - 1, :));
            u_i = u_i / norm(u_i);
            j = cross(u_i, i)/ norm(cross(u_i, i));
            k = cross(i, j);
            
            s_i = dot(u_i,k);
           
            s_t = ( indiceRefraction0/indiceRefractiont * sin(obj.angles(end)));
            %aussi �quivalent a:
            %s_t = indiceRefraction0/indiceRefractiont *s_i;
            obj.angles = cat(1, obj.angles, asin(s_t));
            %direction du vecteur refract�
            u_t = -i * sqrt(1 - power(s_t,2)) +  k * s_t;
            
            while(mustGoOn)
                
                ptsIntersection = [];
                
                for pIdx = 1:length(plansBoites)
                    
                    pl = plansBoites(pIdx);
                    
                    [isIntersection, pointContact] = pl.findIntersection(u_t, obj.positions(height, :));
                    
                    if(isIntersection)
                        
                        ptsIntersection = cat(2, ptsIntersection, pointContact(:));
                        
                    end
                    
                end
                
                deltaLePlusPetit = intmax('int64');
                pointLePlusProche = [NaN;NaN;NaN];
                
                [height width] = size(ptsIntersection);
                
                for pIdx = 1:width
                    pt = ptsIntersection(:, pIdx);
                    vecteurDifference = (pt - transpose(obj.positions(height - 1, :)));
                    
                    delta = norm(vecteurDifference);
                    
                    if(delta < deltaLePlusPetit)
                        
                        deltaLePlusPetit = delta;
                        pointLePlusProche = pt;
                        
                    end
                end
                
                if ~isnan(pointLePlusProche(1))
                         
                    obj.positions = cat(1, obj.positions, transpose(pointLePlusProche));
                   
                end
                [height width] = size(obj.positions);
                
                u = transpose(obj.positions(height, :)...
                    - obj.positions(height - 1, :));
                
                u = u / norm(u);
                
                angleCritique = getCriticalAngle(indiceRefractiont, indiceRefraction0);
                
                for pIdx = 1:length(plansBoites)
                    pl = plansBoites(pIdx);
                    if pl.isPointInPlan(pointLePlusProche)
                        
                        obj.plan = pl;
                        
                        if obj.plan.type == 2
                            
                            mustGoOn = false;
                            obj.isGood = true;
                            
                        else
                            
                            cos_b = abs(dot(obj.plan.normale,u)/norm(u));
                            angleIncident = acos(cos_b);
                            
                            if angleIncident > angleCritique
                                obj.angles = cat(1, obj.angles, obj.angles(end));
                                
                                u_t(obj.plan.axes(3)) = -u_t(obj.plan.axes(3));
                                
                            else
                                
                                mustGoOn = false;
                                
                            end
                            
                        end
                        
                        break;
                    end
                    
                end
                
                compteur = compteur + 1;
                
                if(compteur == 3)
                    mustGoOn = false;
                end
                
            end
            
            
        end
        
        function generateMirrorView(obj)
            
            vecteurDirection = obj.positions(2, :) - obj.positions(1, :);
            vecteurDirection = vecteurDirection / norm(vecteurDirection);
            
            distance = 0;
            
            
            for i = 2:length(obj.positions)
                
                vecteur = obj.positions(i, :) - obj.positions(i-1, :);
                if ~isnan(vecteur)
                    distance = distance + norm(vecteur);
                end
            end
            
            obj.positionFinale = obj.positions(1, :) + (vecteurDirection .* distance);
            
        end
        
    end
    
end

