function [ boxPlans ] = createCenterBox(  )
%CREATETRANSPARENTBOX Summary of this function goes here
%   Detailed explanation goes here

centreBloc = [0.035; 0.04; 0.145];

[a, b ] = createPlan( centreBloc, 0.01, 0.05, PlanEnum.XZ, 0.02/2, 2);
[c, d ] = createPlan( centreBloc, 0.01, 0.02, PlanEnum.XY, 0.05/2, 2);
[e, f ] = createPlan( centreBloc, 0.02, 0.05, PlanEnum.YZ, 0.01/2, 2);


a.couleur = 'y';
b.couleur = 'g';
c.couleur = 'm';
d.couleur = 'b';
e.couleur = 'c';
f.couleur = 'r';

boxPlans = cat(1, f, e, b, a, d, c);

end

