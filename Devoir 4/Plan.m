classdef Plan < handle
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(GetAccess = 'public', SetAccess = 'public')
        %nombrePoints = 50;
        largeur;
        hauteur;
        planParal; % 1 = XY, 2 = XZ, 3 = YZ
        positionCentre;
        precision = 1/1000;
        points;
        normale;
        axes;
        type;
        couleur = 'k';
    end
    
    methods
        
        function obj = Plan(largeur, hauteur, positionCentre, planParal, normale)
            obj.largeur = largeur;
            obj.hauteur = hauteur;
            obj.planParal = planParal;
            obj.positionCentre = positionCentre;
            obj.normale = normale;
            obj.generatePoints();
        end
        
        function pointDepart = getPointDepart(obj)
            
            if(obj.planParal == PlanEnum.XY) % Plan XY
                
                pointDepart(1) = obj.positionCentre(1) - obj.largeur/2;
                pointDepart(2) = obj.positionCentre(2) - obj.hauteur/2;
                pointDepart(3) = obj.positionCentre(3);
                
                obj.axes(1) = 1;
                obj.axes(2) = 2;
                obj.axes(3) = 3;
            elseif( obj.planParal == PlanEnum.XZ) % Plan XZ
                
                pointDepart(1) = obj.positionCentre(1) - obj.largeur/2;
                pointDepart(2) = obj.positionCentre(2);
                pointDepart(3) = obj.positionCentre(3) - obj.hauteur/2;
                
                obj.axes(1) = 1;
                obj.axes(2) = 3;
                obj.axes(3) = 2;
            elseif (obj.planParal == PlanEnum.YZ) % Plan YZ
                
                pointDepart(1) = obj.positionCentre(1);
                pointDepart(2) = obj.positionCentre(2) - obj.largeur/2;
                pointDepart(3) = obj.positionCentre(3) - obj.hauteur/2;
                
                obj.axes(1) = 2;
                obj.axes(2) = 3;
                obj.axes(3) = 1;
            else
                
                pointDepart(1) = 0;
                pointDepart(2) = 0;
                pointDepart(3) = 0;
                disp('disaster');
                
            end
        end
        
        
        function generatePoints(obj)
            
            pointDepart = obj.getPointDepart();
            
            points = [];
            
            for i = obj.precision / 2:obj.precision:obj.largeur
                for j = obj.precision / 2:obj.precision:obj.hauteur
                    
                    point = pointDepart;
                    
                    point(obj.axes(1)) = pointDepart(obj.axes(1)) + i;
                    point(obj.axes(2)) = pointDepart(obj.axes(2)) + j;
                    
                    points = [points; point];
                    
                end
            end
            
            obj.points = points;
            
        end
        function isInPlan = isPointInPlan(obj, point)
            isInPlan = false;
            
            p_0 = obj.positionCentre;
            
            if point(obj.axes(1)) <= p_0(obj.axes(1)) + obj.largeur/2 ...
                    && point(obj.axes(1)) >=  p_0(obj.axes(1)) - obj.largeur/2
                
                if point(obj.axes(2)) <= p_0(obj.axes(2)) + obj.hauteur/2 ...
                        && point(obj.axes(2))>=  p_0(obj.axes(2)) - obj.hauteur/2
                    
                    distance = p_0(obj.axes(3)) - point(obj.axes(3));
                    
                    zero = 10^-7;
                    
                    if(abs(distance) <= zero)
                        isInPlan  = true;
                    end
                end
            end
            
        end
        %https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
        function [intersect point] = findIntersection(obj, l, l_0)
            intersect = false;
            point = [NaN;NaN;NaN];
            
            p_0 = obj.positionCentre;
            n = obj.normale;
            
            zero = 10^-7;
            
            if( abs(dot(l,n)) > zero && ~obj.isPointInPlan(l_0))
                d =  (dot(p_0-l_0,n)) / (dot(l,n));
                point = d*l + transpose(l_0);
                
                switch(obj.planParal)
                    case PlanEnum.XY
                        point(3) = p_0(3); 
                    case PlanEnum.XZ
                        point(2) = p_0(2);
                    case PlanEnum.YZ
                        point(1) = p_0(1);
                    otherwise
                        disp('disaster! findIntersection');
                end
                
                
                intersect = obj.isPointInPlan(point);
                    
                
            end   
        end
    end
    
end

