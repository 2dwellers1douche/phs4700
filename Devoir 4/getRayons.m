function rayons = getRayons(tetaCritique, plans, posObs)

rayons = [];

for i =1:numel(plans)
    plan = plans(i);
    points = plan.points;
    
    [height width] = size(points);
    
    for j = 1:height
       point = points(j,:);
        
       u = transpose(point)-posObs;
       n = plan.normale;
       cos_b = abs(dot(n,u)/norm(u));
       
       
       angle = acos(cos_b);

       %rad2deg(angle)
        
        %vectObsPointUnitaire = dot(vecteurObsPoint, plan.normale) / norm(vecteurObsPoint);
        
        %angle = abs(pi / 2 - real(acos(vectObsPointUnitaire)));
        
        if abs(angle) < tetaCritique
            
            r = Rayon(posObs, point, angle, plan);
            
            rayons = cat(1, rayons, r);
        end
    end


end


end