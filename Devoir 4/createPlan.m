function [ plan1, plan2 ] = createPlan( pointsCentre3D, largeur, hauteur, axe, distanceAuCentre, type )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
% axe: 1 = XY, 2 = XZ, 3 = YZ
% Plan(largeur, hauteur, planParal, positionCentre);


switch axe
    case PlanEnum.XY
        
        positionCentrePlanTop = pointsCentre3D(3) + distanceAuCentre;
        positionCentrePlanBot = pointsCentre3D(3) - distanceAuCentre;

        plan1 = Plan(largeur, hauteur,...
            [pointsCentre3D(1), pointsCentre3D(2), positionCentrePlanTop],...
            PlanEnum.XY, [0; 0; 1]);
        plan2 = Plan(largeur, hauteur, ...
            [pointsCentre3D(1), pointsCentre3D(2), positionCentrePlanBot], ...
            PlanEnum.XY, [0; 0; -1]);
        
    case PlanEnum.XZ
        
        positionCentrePlan1 = pointsCentre3D(2) + distanceAuCentre;
        positionCentrePlan2 = pointsCentre3D(2) - distanceAuCentre;

        plan1 = Plan(largeur, hauteur, ...
            [pointsCentre3D(1), positionCentrePlan1, pointsCentre3D(3)], ...
            PlanEnum.XZ, [0; 1; 0]);
        plan2 = Plan(largeur, hauteur, ...
            [pointsCentre3D(1), positionCentrePlan2, pointsCentre3D(3)], ...
            PlanEnum.XZ, [0; -1; 0]);
        
    case PlanEnum.YZ
        
        positionCentrePlan1 = pointsCentre3D(1) + distanceAuCentre;
        positionCentrePlan2 = pointsCentre3D(1) - distanceAuCentre;

        plan1 = Plan(largeur, hauteur, ...
            [positionCentrePlan1, pointsCentre3D(2), pointsCentre3D(3)], ...
            PlanEnum.YZ, [1; 0; 0]);
        plan2 = Plan(largeur, hauteur, ...
            [positionCentrePlan2, pointsCentre3D(2), pointsCentre3D(3)], ...
            PlanEnum.YZ, [-1; 0; 0]);
        
    otherwise
        
        disp('disaster\n');
end

plan1.type = type;
plan2.type = type;

end

