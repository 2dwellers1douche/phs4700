function [ boxPlans ] = createTransparentBox(  )
%CREATETRANSPARENTBOX Summary of this function goes here
%   Detailed explanation goes here

centreBlocTransparent = [0.035; 0.035; 0.125];

[a, b ] = createPlan( centreBlocTransparent, 0.07, 0.15, PlanEnum.XZ, 0.07/2, 1);
[c, d ] = createPlan( centreBlocTransparent, 0.07, 0.07, PlanEnum.XY, 0.15/2, 1);
[e, f ] = createPlan( centreBlocTransparent, 0.07, 0.15, PlanEnum.YZ, 0.07/2, 1);


boxPlans = cat(1, f, e, b, a, d, c);

end

