function [ ] = drawEverything( posObs, plans, rayons, middleBoxPlans )
%DRAWEVERYTHING Summary of this function goes here
%   Detailed explanation goes here

totalGoodRayon = 0;

dessinePlan = true;
dessineBoiteTransparante = true;
dessineRayonsFinaux = false;
dessineRayons = true;
figure
hold on;
axis equal;
xlabel('x (m)')
ylabel('y (m)')
zlabel('z (m)')

%dessine l'observateur
scatter3(posObs(1),posObs(2),posObs(3),'x')

%dessine les plans

if dessinePlan
    for i = 1 : numel(plans)
        p = plans(i);
        
        x = p.points(:,1);
        y = p.points(:,2);
        z = p.points(:,3);
        
        scatter3(x,y,z,'.', 'k')
    end
end

if dessineBoiteTransparante
    for i = 1 : numel(middleBoxPlans)
        p = middleBoxPlans(i);
        
        x = p.points(:,1);
        y = p.points(:,2);
        z = p.points(:,3);
        
        scatter3(x,y,z,'.', 'r')
    end
end

%dessine la reflexion finale
if dessineRayonsFinaux
    for i = 1 : length(rayons)
        rayon = rayons(i);
        
        color = rayon.plan.couleur;
        
        
        if rayon.isGood == true
            totalGoodRayon = totalGoodRayon + 1;
            x = rayon.positionFinale(1);
            y = rayon.positionFinale(2);
            z = rayon.positionFinale(3);
            
            scatter3(x,y,z, '.', color);
        end
    end
end

if dessineRayons
    for i = 1 : length(rayons)
        rayon = rayons(i);
         if rayon.isGood == true
            totalGoodRayon = totalGoodRayon + 1;
         end
        %if rayon.isGood == true
            x = rayon.positions(:, 1);
            y = rayon.positions(:, 2);
            z = rayon.positions(:, 3);
            
            line(x,y,z);
        %end
    end
end

disp(strcat(strcat(num2str(totalGoodRayon),'/', num2str(length(rayons)))));

hold off;


end

