clc;
clear;
close all;

idxObs = 2;
idxN = 1;
% ,[0.035;0.035;0.27]
posObservateur = [[-0.10;-0.10;0.15], [0.13;0.10;0.25]];

n0 = [1; 1.33];
nt = [1.5; 1.1];

boxPlans = createTransparentBox();
middleBoxPlans = createCenterBox();


for idxN = 1:2
    
    for idxObs = 1:2
        plansVus = getPlanVu(posObservateur(:,idxObs));
        
        %  calcul d'angle critique avec la surface
        tetaCritique = getCriticalAngle(n0(idxN), nt(idxN));
        
        %  elimination des points qui ne rentrent pas a l'interieur
        rayons = getRayons(tetaCritique, boxPlans(plansVus(:)), posObservateur(:,idxObs));
        
        %  calcul des angles entrants dans la boite transparante
        
        for i = 1 : length(rayons)
            r = rayons(i);
            
            % Refraction angle (Snell's law)
            
            r.go(n0(idxN), nt(idxN), cat(1, boxPlans, middleBoxPlans));
            
            r.generateMirrorView();
        end
        
        
        
        drawEverything(posObservateur(:,idxObs), boxPlans(plansVus(:)), rayons, middleBoxPlans);
        drawnow
    end
end