function [ indexFaces ] = getPlanVu( pos )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
ptMilieu = [0.035, 0.035, 0.125];

largeur = 0.07; %m
hauteur = 0.15; %m

indexFaces = [];

if( pos(2) < ptMilieu(2) - largeur/2)
    indexFaces = [indexFaces, 1]; %rouge
end

if( pos(2) > ptMilieu(2) + largeur/2)
    indexFaces = [indexFaces, 2]; %cyan
end


if( pos(1) < ptMilieu(1) - largeur/2)
    indexFaces = [indexFaces, 3]; %verte
end

if( pos(1) > ptMilieu(1) + largeur/2)
    indexFaces = [indexFaces, 4]; %jaune
end

if( pos(3) < ptMilieu(3) - hauteur/2)
    indexFaces = [indexFaces, 5]; %bleue
end

if( pos(3) > ptMilieu(3) + hauteur/2)
    indexFaces = [indexFaces, 6]; %magenta
end
end

