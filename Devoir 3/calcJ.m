function [ j ] = calcJ( balle, bdc,normale, positionRelativeBalle, positionRelativeBdc )
%CALCJ Summary of this function goes here
%   Detailed explanation goes here
epsilon = 0.5;

rapn = cross(positionRelativeBalle, normale);
rapnrap = cross(rapn, positionRelativeBalle);

rbpn = cross(positionRelativeBdc, normale);
rbpnrbp = cross(rbpn, positionRelativeBdc);

matRot = bdc.getMatrixRotation();

Gballe = dot(normale, inv(balle.getMomentInertie())*rapnrap );
Gbdc = dot(normale, (matRot*inv(bdc.getMomentInertie()))*rbpnrbp);

alpha = 1 / ( ...
    (1/balle.masse) +...
    (1/bdc.masse) +...
    Gballe+Gbdc);

vrmoins = dot(...
    normale, ...
    ( balle.getVitesseRelative(positionRelativeBalle) - ...
    bdc.getVitesseRelative(positionRelativeBdc) ));

j = -alpha*(1+epsilon) * vrmoins;
end

