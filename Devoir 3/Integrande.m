function acceleration_objet = Integrande(vitesse, objet)

masse_objet = objet.masse; %kg
force_frottement_visqueux = objet.getforceFrottementVisqueux(vitesse);

F_gravitationnelle = [0; 0; -9.81].* masse_objet;
%a = Ftotale/m
acceleration_objet = (force_frottement_visqueux + F_gravitationnelle)./masse_objet;
end

