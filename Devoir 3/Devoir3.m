function [Dev, vbaf, vbof, rba, rbo, tc] = Devoir3(wboitei,vballei,tballe)

%init de la simulation
Dev = false;
vbaf = zeros(2,6);
vbof = zeros(2,6);
rba = [];
rbo = [];
tc = [0];

epsillon = 0.0005; %metre

dt = 1*10^(-6); %seconde
balle = Balle(  0.0335 , ...
    0.058, ...
    [0;0;2],...
    vballei);

bdc   = BoiteDeConserve(0.15,...
    0.05,...
    75/1000,...
    [3;0;10],...
    [0;0;0],...
    wboitei);

doitRouler = true;
ancienneVitesseBalle = [0;0;0];
ancienneVitesseBdc = [0;0;0];   
pointCollision = [0;0;0];
tc = [0];
balle.positions = [balle.position];
bdc.positions = [bdc.position];
bdc.rotations = [bdc.rotation];
rba = [(balle.position)];
rbo = [(bdc.position)];
nonPrecis = false;
while(doitRouler)
    tc = [tc tc(end)+dt];
    
    
    %% Deplacement de la balle
    if(tballe <= 0)
        ancienneVitesseBalle = balle.vitesse;
        balle.vitesse = SEDRK4(balle.vitesse, dt, 'Integrande', balle);
        balle.position = balle.position + balle.vitesse.* dt;
        
        %pour nos test de précision
        %on trouve lal ongueur du deplacement
%         delta_position = balle.position - ancienneVitesseBalle;
%         magnitude_delta = norm(delta_position);
%         
%         if(magnitude_delta >= epsillon && nonPrecis == false) %si le déplacement est trop grand
%             fprintf('NON PRECIS!')   
%             nonPrecis = true;
%         end

    else
        tballe = tballe-dt;
    end
    
    %% Deplacement de la boite de conserve
    ancienneVitesseBdc = bdc.vitesse;
    bdc.vitesse = SEDRK4(bdc.vitesse, dt, 'Integrande', bdc);
    bdc.position = bdc.position + bdc.vitesse.* dt;
    bdc.rotation = bdc.rotation + bdc.vitesseAngulaireInitiale * dt;
    
    rba = [rba (balle.position)];
    rbo = [rbo (bdc.position)];
    
    %pour l'animation
    balle.positions = [balle.positions balle.position];
    bdc.positions = [bdc.positions bdc.position];
    bdc.rotations = [bdc.rotations bdc.rotation];
    
    %% tests sur les nouvelles positions
    if( balle.position(3) <= balle.rayon)
        %% Si la balle touche le sol
        doitRouler = false;
        
        vbaf(1,1) = balle.vitesse(1);
        vbaf(1,2) = balle.vitesse(2);
        vbaf(1,3) = balle.vitesse(3);
        vbaf(1,4) = balle.vitesseAngulaireFinale(1);
        vbaf(1,5) = balle.vitesseAngulaireFinale(2);
        vbaf(1,6) = balle.vitesseAngulaireFinale(3);
        
        vbaf(2,1) = balle.vitesse(1);
        vbaf(2,2) = balle.vitesse(2);
        vbaf(2,3) = balle.vitesse(3);
        vbaf(2,4) = balle.vitesseAngulaireFinale(1);
        vbaf(2,5) = balle.vitesseAngulaireFinale(2);
        vbaf(2,6) = balle.vitesseAngulaireFinale(3);
        
        vbof(1,1) = bdc.vitesse(1);
        vbof(1,2) = bdc.vitesse(2);
        vbof(1,3) = bdc.vitesse(3);
        vbof(1,4) = bdc.vitesseAngulaireFinale(1);
        vbof(1,5) = bdc.vitesseAngulaireFinale(2);
        vbof(1,6) = bdc.vitesseAngulaireFinale(3);
        
        vbof(2,1) = bdc.vitesse(1);
        vbof(2,2) = bdc.vitesse(2);
        vbof(2,3) = bdc.vitesse(3);
        vbof(2,4) = bdc.vitesseAngulaireFinale(1);
        vbof(2,5) = bdc.vitesseAngulaireFinale(2);
        vbof(2,6) = bdc.vitesseAngulaireFinale(3);
        
        rba = [rba (balle.position)];
        rbo = [rbo (bdc.position)];
    else
        %% Si la balle est dans les airs
        
        %on regarde si les nouvelles positions engendrent une collision
        [enCollision pointCollision normale] = balle.getCollision(bdc);
        
        if(enCollision == true)
            %Trouver les résultantes
            pointCollision
            apresCollision(balle, bdc, normale, pointCollision);
            vbaf(1,1) = ancienneVitesseBalle(1);
            vbaf(1,2) = ancienneVitesseBalle(2);
            vbaf(1,3) = ancienneVitesseBalle(3);
            vbaf(1,4) = balle.vitesseAngulaireInitiale(1);
            vbaf(1,5) = balle.vitesseAngulaireInitiale(2);
            vbaf(1,6) = balle.vitesseAngulaireInitiale(3);
            
            vbaf(2,1) = balle.vitesse(1);
            vbaf(2,2) = balle.vitesse(2);
            vbaf(2,3) = balle.vitesse(3);
            vbaf(2,4) = balle.vitesseAngulaireFinale(1);
            vbaf(2,5) = balle.vitesseAngulaireFinale(2);
            vbaf(2,6) = balle.vitesseAngulaireFinale(3);
            
            vbof(1,1) = ancienneVitesseBdc(1);
            vbof(1,2) = ancienneVitesseBdc(2);
            vbof(1,3) = ancienneVitesseBdc(3);
            vbof(1,4) = bdc.vitesseAngulaireInitiale(1);
            vbof(1,5) = bdc.vitesseAngulaireInitiale(2);
            vbof(1,6) = bdc.vitesseAngulaireInitiale(3);
            
            vbof(2,1) = bdc.vitesse(1);
            vbof(2,2) = bdc.vitesse(2);
            vbof(2,3) = bdc.vitesse(3);
            vbof(2,4) = bdc.vitesseAngulaireFinale(1);
            vbof(2,5) = bdc.vitesseAngulaireFinale(2);
            vbof(2,6) = bdc.vitesseAngulaireFinale(3);
            
            rba = [rba (balle.position)];
            rbo = [rbo (bdc.position)];
            
            doitRouler = false;
            Dev = true;
        end
    end
    
end

%le dessins des plots
% if(true)
%     symboles =  ['r.','g+','bo'];
% 
%     figure
%     title('Simulation');
%     xlabel('x (m)')
%     ylabel('y (m)')
%     zlabel('z (m)')
%     axis equal;
%     axis([0 4 -1 1 0 5 0 1])
%     hold on
%     [x,y,z] = sphere;
%     x = x .* balle.rayon;
%     y = y .* balle.rayon;
%     z = z .* balle.rayon;
%     
%     [xc,yc,zc] = cylinder(bdc.rayon);
%     zc = zc - 0.5;
%     zc = zc * bdc.hauteur;
%     
%     t = hgtransform;
%     
%     mesh(x + balle.position(1),y + balle.position(2),z + balle.position(3))
%     mesh(xc,yc,zc,'Parent',t)
%     
%     rx_angle = bdc.rotation(1);
%     ry_angle = bdc.rotation(2);
%     rz_angle = bdc.rotation(3);
%     Rx = makehgtform('yrotate',rx_angle);
%     Ry = makehgtform('yrotate',ry_angle);
%     Rz = makehgtform('yrotate',rz_angle);
%     Tx1 = makehgtform('translate',[bdc.position(1) bdc.position(2) bdc.position(3)]);
%     
%     t.Matrix = Tx1* (Rx*Ry*Rz);
%     
%     plot3(pointCollision(1),pointCollision(2),pointCollision(3),'bo')
%     plot3(balle.positions(1,end),balle.positions(2,end),balle.positions(3,end),'r.')
%     plot3(bdc.positions(1,end),bdc.positions(2,end),bdc.positions(3,end),'g+')
%     
%     plot3(balle.positions(1,1:10:length(balle.positions)),balle.positions(2,1:10:length(balle.positions)),balle.positions(3,1:10:length(balle.positions)),'r.')
%     plot3(bdc.positions(1,1:10:length(bdc.positions)),bdc.positions(2,1:10:length(bdc.positions)),bdc.positions(3,1:10:length(bdc.positions)),'r.')
%     
%     
%     
%     
%     
%     
%     hold off
% end

end