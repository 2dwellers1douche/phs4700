function [ ] = apresCollision( balle, bdc, normale, pdc)


positionRelativeBalle = pdc - balle.position;
positionRelativeBdc   = pdc - bdc.position  ;


matRotBdc = bdc.getMatrixRotation();

vInitBdcRelative = ...
bdc.vitesse + ...
matRotBdc*cross(bdc.vitesseAngulaireInitiale, positionRelativeBdc);

vInitBalleRelative = ...
    balle.vitesse + ...
    cross(balle.vitesseAngulaireInitiale, positionRelativeBalle);


j = ...
    calcJ(...
    balle, bdc,normale, ...
    positionRelativeBalle, positionRelativeBdc);


rapn = cross(positionRelativeBalle, normale);
rapnrap = cross(rapn, positionRelativeBalle);

rbpn = cross(positionRelativeBdc, normale);
rbpnrbp = cross(rbpn, positionRelativeBdc);


watf = balle.vitesseAngulaireInitiale + ...
    j *(inv(balle.getMomentInertie()) * ...
    rapn);

wbtf = bdc.vitesseAngulaireInitiale - ...
    j *((inv(bdc.getMomentInertie())) * ...
    rbpn);

vaptf = vInitBalleRelative + ...
    j * ( ...
    (normale/balle.masse) + inv( balle.getMomentInertie()) * ...
    rapnrap);  %aka vapt

vbptf = vInitBdcRelative - ...
    j * ( ...
    (normale/bdc.masse) + (matRotBdc*inv(bdc.getMomentInertie())) * ...
    rbpnrbp);%aka vbpt


vat = vaptf - ( ...
    cross(watf, positionRelativeBalle));

vbt = vbptf - ( ...
    matRotBdc*cross(wbtf, positionRelativeBdc));




balle.vitesseAngulaireFinale = watf;
balle.vitesse = vat;

bdc.vitesseAngulaireFinale = wbtf;
bdc.vitesse = vbt;

end
