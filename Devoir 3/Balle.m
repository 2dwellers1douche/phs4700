classdef Balle < handle
    %BALLE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(GetAccess = 'public', SetAccess = 'public')
        rayon; %metre
        masse; %kg
        position; %metre
        vitesse; %m/s
        vitesseAngulaireInitiale;
        vitesseAngulaireFinale;
        k = 0.1; %kg/(m^2*s)
        positions;
    end
    
    methods
        function obj = Balle(rayon,masse,position,vitesse)
            obj.rayon = rayon;
            obj.masse = masse;
            obj.position = position;
            obj.vitesse = vitesse;
            obj.vitesseAngulaireInitiale = [0;0;0];
            obj.vitesseAngulaireFinale = [0;0;0];
        end
           
        function facteurA = getFacteurA(obj)
            facteurA = pi * obj.rayon^2;
        end
        
        function forceFrottementVisqueux = getforceFrottementVisqueux(obj, vitesse)
            forceFrottementVisqueux = -obj.k*obj.getFacteurA()*vitesse;
        end
        
        
        function [enCollision, pointCollision normale] = getCollision(obj, bdc)
            enCollision = 0;
            pointCollision = [0;0;0];
            normale = [0;0;0];
            pointLePlusProche = [0;0;0];
            %premi�re �tape est de remettre le monde comme si le cylindre
            %est � [0;0;0]
            sphereOrigin = obj.position - bdc.position;
            sphereOrigin2D = [sphereOrigin(1);sphereOrigin(2)];
            %on applique la rotation inverse
            sphereOrigin = inv(bdc.getMatrixRotation()) * sphereOrigin;
            
            %on regarde si la balle intersecte avec un cylindre de hauteur
            %infinie du rayon de la boite de conserve.
            distanceAOrigine = norm([sphereOrigin(1), sphereOrigin(2)]);
            if(isnan(distanceAOrigine))
                distanceAOrigine = 0;
            end
            if( distanceAOrigine <= obj.rayon + bdc.rayon)
                %premier cas trivial. Si le centre de la balle est dans le
                %cylindre
                
                if(abs(sphereOrigin(3)) <= 0.5*bdc.hauteur)
                    %Il y a une collision
                    pointLePlusProche = sphereOrigin /norm(sphereOrigin) * bdc.rayon;
                    pointLePlusProche(3) = sphereOrigin(3)
                    enCollision = 1;     
                    
                    pointCollision = bdc.getMatrixRotation() * pointLePlusProche + bdc.position;

                    normale = obj.position - pointCollision;
                else 
                    %on regarde si la balle, la ou elle est moins large,
                    %est en collision avec le cylindre
                    
                    %on trouve le point le plus proche du centre de la
                    %balle sur le cylindre
                    
                    if(distanceAOrigine > bdc.rayon)
                         pointLePlusProche = sphereOrigin /norm(sphereOrigin) * bdc.rayon;
                    else
                        pointLePlusProche = sphereOrigin; 
                    end
                    
                    if(sphereOrigin(3) < 0)
                        pointLePlusProche(3) = -bdc.hauteur/2;
                        normale = [0;0;-1];
                    else
                        pointLePlusProche(3) = bdc.hauteur/2;
                        normale = [0;0;1];
                    end
                    
                    %on compare la distance entre ce point et le centre de
                    %la sphere pour voir si une collision a lieu
                    if(norm(sphereOrigin - pointLePlusProche) <= obj.rayon)
                        enCollision = 1;
                        pointCollision = bdc.getMatrixRotation()*pointLePlusProche + bdc.position; 
                        
                        
                    end
                end         
            end 
            
            %Dans le cas ou il ya a une collision on calcule la normale
            if(enCollision == 1)
               %on rotate la normal
               normale = bdc.getMatrixRotation() * normale;
               normale = normale/norm(normale);
            end
        end
        
        function momentInertie = getMomentInertie(obj)
             moment = (2/5)*obj.masse*(obj.rayon^2);
            
            momentInertie = [
                moment 0 0;
                0 moment 0;
                0 0 moment;
            ];
        end
        
        function vitesseRelative = getVitesseRelative(obj, positionRelative)
            vitesseRelative = obj.vitesse + cross(obj.vitesseAngulaireInitiale, positionRelative);
        end
    end
end


