classdef BoiteDeConserve < handle
    %BALLE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(GetAccess = 'public', SetAccess = 'public')
        hauteur; %metre
        rayon; %metre
        masse; %kg
        position; %metre
        vitesse; %m/s
        k = 0.1; %kg/(m^2*s)
        vitesseAngulaireInitiale;%rad/s
        vitesseAngulaireFinale;
        rotation; %rad
        positions = [];
        rotations = [];
    end
    
    methods
        function obj = BoiteDeConserve(hauteur,rayon,masse,position,vitesse,vitesseAngulaire)
            obj.hauteur = hauteur;
            obj.rayon = rayon;
            obj.masse = masse;
            obj.position = position;
            obj.vitesse = vitesse;
            obj.vitesseAngulaireInitiale = vitesseAngulaire;
            obj.vitesseAngulaireFinale = vitesseAngulaire;
            obj.rotation = [0;0;0]
        end
        
        function matRot = getMatrixRotation(obj)
            rot = obj.rotation;
            
            a = rot(1);
            rx = [
                    1   0   0;
                    0   cos(a) -sin(a);
                    0   sin(a) cos(a)
                    ];
                  
            b = rot(2);
            ry = [
                   cos(b) 0 sin(b);
                   0      1     0;
                   -sin(b)  0   cos(b)
                   ];
               
            c = rot(3);
             rz = [
                       cos(c) -sin(c) 0;
                       sin(c) cos(c)  0;
                       0    0   1;
                    ];
            matRot = rx * ry * rz;
        end
           
        function facteurA = getFacteurA(obj)
            facteurA = obj.rayon^2 + obj.hauteur^2;
        end
        
        function forceFrottementVisqueux = getforceFrottementVisqueux(obj,vitesse)
            forceFrottementVisqueux = -obj.k*obj.getFacteurA()*vitesse;
        end
        
        function momentInertie = getMomentInertie(obj)
            
           momentzz = obj.masse*(obj.rayon^2);
           
           momentxxyy = ...
               (1/2)*obj.masse*(obj.rayon^2) + ...
               (1/12)*obj.masse*(obj.hauteur^2);
            
            momentInertie = [
                momentxxyy 0 0;
                0 momentxxyy 0;
                0 0 momentzz;
            ];
        
        end
        
        function vitesseRelative = getVitesseRelative(obj, positionRelative)
            vitesseRelative = obj.vitesse + obj.getMatrixRotation()*cross(obj.vitesseAngulaireInitiale, positionRelative);
        end
    end
    
end