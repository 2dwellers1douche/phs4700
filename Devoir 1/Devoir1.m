clear all;
clc;
% Mesures en metre et poids en kg


% Jambes
jambeRayon          = 0.06 ; % 6 cm
jambeLongueur       = 0.75 ; % 75 cm
jambeMasseVolumique = 1052.0 ; % 1052 kg / m^3
jambeGaucheCentreMasse    = [-0.1; 0.0; jambeLongueur/2.0];
jambeDroiteCentreMasse    = [0.1; 0.0; jambeLongueur/2.0];
jambeVolume = pi * jambeRayon^2 * jambeLongueur ;
jambeMasse  = jambeVolume * jambeMasseVolumique ;

% Tronc
troncRayon          = 0.15 ;
troncLongueur       = 0.7 ;
troncMasseVolumique = 953.0 ;
troncCentreMasse    = [0.0; 0.0; jambeLongueur + troncLongueur/2.0];
troncVolume = pi * troncRayon^2 * troncLongueur;
troncMasse = troncVolume * troncMasseVolumique ;

% Cou
couRayon            = 0.04 ; 
couLongueur         = 0.1 ;
couMasseVolumique   = troncMasseVolumique ;
couCentreMasse      = [0.0; 0.0; jambeLongueur + troncLongueur + couLongueur/2.0];
couVolume = pi * couRayon^2 * couLongueur ;
couMasse  = couVolume * couMasseVolumique ;

% Tete
teteRayon           = 0.10 ;
teteMasseVolumique  = 1056.0 ;
teteCentreMasse     = [0.0; 0.0; jambeLongueur + troncLongueur + couLongueur + teteRayon]; 
teteVolume = (4*pi*teteRayon^3) / 3.0 ;
teteMasse  = teteVolume * teteMasseVolumique ;

% Bras
brasRayon    = 0.03 ;
brasLongueur = 0.75 ; 
brasMasseVolumique = 1052.0 ;
brasVolume = pi * brasRayon ^2 * brasLongueur;
brasMasse = brasVolume * brasMasseVolumique;
brasDroitCentreMasseVertical    = [(troncRayon + brasRayon);  0.0; jambeLongueur + troncLongueur - brasLongueur/2];
brasGaucheCentreMasseVertical   = [-(troncRayon + brasRayon); 0.0; jambeLongueur + troncLongueur - brasLongueur/2];
brasGaucheCentreMasseHorizontal = [-(troncRayon + brasLongueur/2.0); 0.0; jambeLongueur + troncLongueur - brasRayon];
%%
% a) Position du centre de masse de chacun des patineurs

sommeDesMasses =  ...
    2*jambeMasse + ...
    troncMasse + ...
    couMasse + ...
    teteMasse + ...
    2*brasMasse 

% sans le bras leve

sommeDesCentreDeMasseVertical ...
    = jambeMasse*jambeGaucheCentreMasse +...
      jambeMasse*jambeDroiteCentreMasse + ...
      troncMasse*troncCentreMasse + ...
      couMasse*couCentreMasse + ...
      teteMasse*teteCentreMasse + ...
      brasMasse*brasDroitCentreMasseVertical + ...
      brasMasse*brasGaucheCentreMasseVertical ;
      
centreDeMasseTotalVertical = (1/sommeDesMasses)*(sommeDesCentreDeMasseVertical)

% avec le bras leve
sommeDesCentreDeMasseHorizontal ...
    = jambeMasse*jambeGaucheCentreMasse +...
      jambeMasse*jambeDroiteCentreMasse + ...
      troncMasse*troncCentreMasse + ...
      couMasse*couCentreMasse + ...
      teteMasse*teteCentreMasse + ...
      brasMasse*brasDroitCentreMasseVertical + ...
      brasMasse*brasGaucheCentreMasseHorizontal ;
  centreDeMasseTotalHorizontal = (1/sommeDesMasses)*(sommeDesCentreDeMasseHorizontal)
  
%% b) Moment d'inertie pour cacun des patineurs par rapport a son centre de masse

% Jambes (cylindres pleins : Ic = (mr^2/4 + ml^2/12, mr^2/4 + ml^2/12, mr^2/2)
% Par rapport a leur centre de masse
m = jambeMasse;
r = jambeRayon;
l = jambeLongueur;
jambesMomentInertie = MomentInertieCylindrePlein(m,r,l);

% par rapport au centre de masse du patineur

% jambe gauche
dc = centreDeMasseTotalVertical - jambeGaucheCentreMasse;
Ic = jambesMomentInertie;
Id = Ic + m*Tdc(dc);
jambeGaucheId = Id;

%jambe droite 
dc = centreDeMasseTotalVertical - jambeDroiteCentreMasse;
Ic = jambesMomentInertie;
Id = Ic + m*Tdc(dc);
jambeDroiteId = Id;

% Tronc
% Par rapport a son centre de masse
m = troncMasse;
r = troncRayon;
l = troncLongueur;
troncMomentInertie = MomentInertieCylindrePlein(m,r,l);

%Par rapport au centre de masse total
dc = centreDeMasseTotalVertical - troncCentreMasse;
Ic = troncMomentInertie;
Id = Ic + m*Tdc(dc);
troncId = Id;

% Cou
% Par rapport a son centre de masse
m = couMasse;
r = couRayon;
l = couLongueur;
couMomentInertie = MomentInertieCylindrePlein(m,r,l);

%Par rapport au centre de masse total
dc = centreDeMasseTotalVertical - couCentreMasse;
Ic = couMomentInertie;
Id = Ic + m*Tdc(dc);
couId = Id;

% Tete
% par rapport a son centre de masse
m = teteMasse;
r = teteRayon;
teteMomentInertie = MomentInertieSpherePleine(m,r);

% par rapport au centre de masse total
dc = centreDeMasseTotalVertical - teteCentreMasse;
Ic = teteMomentInertie;
Id = Ic + m*Tdc(dc);
teteId = Id;


% Bras (cylindres pleins : Ic = (mr^2/4 + ml^2/12, mr^2/4 + ml^2/12, mr^2/2)
% Par rapport a leur centre de masse

% Bras droit
m = brasMasse;
r = brasRayon;
l = brasLongueur;
brasMomentInertie = MomentInertieCylindrePlein(m,r,l);

dc = centreDeMasseTotalVertical - brasDroitCentreMasseVertical;
Ic = brasMomentInertie;
Id = Ic + m*Tdc(dc);
brasDroitId = Id;

% Bras gauche vertical 
dc = centreDeMasseTotalVertical - brasGaucheCentreMasseVertical;

Id = Ic + m*Tdc(dc);
brasGaucheVerticalId = Id;

% Bras gauche horizontal
brasMomentInertie = MomentInertieCylindrePleinAxeY(m,r,l);

dc = centreDeMasseTotalHorizontal - brasGaucheCentreMasseHorizontal;
Ic = brasMomentInertie;
Id = Ic + m*Tdc(dc);
brasGaucheHorizontalId = Id;

%Les moments d'inerties pour les patineurs
patineurVerticalId = jambeGaucheId + jambeDroiteId + troncId + couId + teteId + brasDroitId + brasGaucheVerticalId
patineurHorizontalId = jambeGaucheId + jambeDroiteId + troncId + couId + teteId + brasDroitId + brasGaucheHorizontalId

%% c) Accélération angulaire du patineur initialement au repos
% On applique une force de F = (0, -200, 0)^T N au point r_f(0, r_h, z_f)^T
% avec z_f = L_j + L_t + L_c + r_h
F = [0; -200; 0];
z_f = jambeLongueur + troncLongueur + couLongueur + teteRayon;
r_f = [0; teteRayon; z_f];
w_initial = [0;0;0];
alphaNormal = AccelerationAngulaire(F,r_f,centreDeMasseTotalVertical,w_initial,patineurVerticalId)

F = [0; -200; 0];
z_f = jambeLongueur + troncLongueur + couLongueur + teteRayon;
r_f = [0; teteRayon; z_f];
w_initial = [0;0;0];
alphaNormalBrasLeve = AccelerationAngulaire(F,r_f,centreDeMasseTotalVertical,w_initial,patineurHorizontalId)

%% d) Accélération angulaire du patineur 
% vitesse angulaire de w = (0, 0, 10)^T rad/s autour de son centre de masse
% 
F = [0; -200; 0];
z_f = jambeLongueur + troncLongueur + couLongueur + teteRayon;
r_f = [0; teteRayon; z_f];
w_initial = [0;0;10];
alphaNOrmalNonRepos = AccelerationAngulaire(F,r_f,centreDeMasseTotalVertical,w_initial,patineurVerticalId)

F = [0; -200; 0];
z_f = jambeLongueur + troncLongueur + couLongueur + teteRayon;
r_f = [0; teteRayon; z_f];
w_initial = [0;0;10];
alphaNormalBrasLeveNonRepos = AccelerationAngulaire(F,r_f,centreDeMasseTotalVertical,w_initial,patineurHorizontalId)

%% incline
theta = -pi/18;
r_y = [ cos(theta)  0   sin(theta);
        0           1   0;
        -sin(theta) 0   cos(theta)];

% Jambes
jambeGaucheCentreMasse    = r_y*[-0.1; 0; jambeLongueur/2];
jambeDroiteCentreMasse    = r_y*[0.1;  0; jambeLongueur/2];

% Tronc
troncCentreMasse    = r_y*[0; 0; jambeLongueur + troncLongueur/2];

% Cou
couCentreMasse      = r_y*[0; 0; jambeLongueur + troncLongueur + couLongueur/2];

% Tete
teteCentreMasse     = r_y*[0; 0; jambeLongueur + troncLongueur + couLongueur + teteRayon]; 

% Bras
brasDroitCentreMasseVertical    = r_y*[(troncRayon + brasRayon);  0; jambeLongueur + troncLongueur - brasLongueur/2];
brasGaucheCentreMasseVertical   = r_y*[-(troncRayon + brasRayon); 0; jambeLongueur + troncLongueur - brasLongueur/2];
brasGaucheCentreMasseHorizontal = r_y*[-(troncRayon + brasLongueur/2); 0; jambeLongueur + troncLongueur - brasRayon];
%%
% a) Position du centre de masse de chacun des patineurs

sommeDesMasses =  ...
    2*jambeMasse + ...
    troncMasse + ...
    couMasse + ...
    teteMasse + ...
    2*brasMasse ;

% sans le bras leve

sommeDesCentreDeMasseVertical ...
    = jambeMasse*jambeGaucheCentreMasse +...
      jambeMasse*jambeDroiteCentreMasse + ...
      troncMasse*troncCentreMasse + ...
      couMasse*couCentreMasse + ...
      teteMasse*teteCentreMasse + ...
      brasMasse*brasDroitCentreMasseVertical + ...
      brasMasse*brasGaucheCentreMasseVertical ;

centreDeMasseTotalVertical = (1/sommeDesMasses)*(sommeDesCentreDeMasseVertical)


% avec le bras leve
sommeDesCentreDeMasseHorizontal ...
    = jambeMasse*jambeGaucheCentreMasse +...
      jambeMasse*jambeDroiteCentreMasse + ...
      troncMasse*troncCentreMasse + ...
      couMasse*couCentreMasse + ...
      teteMasse*teteCentreMasse + ...
      brasMasse*brasDroitCentreMasseVertical + ...
      brasMasse*brasGaucheCentreMasseHorizontal ;
  centreDeMasseTotalHorizontal = (1/sommeDesMasses)*(sommeDesCentreDeMasseHorizontal)
  
%% b) Moment d'inertie pour cacun des patineurs par rapport a son centre de masse

% Jambes (cylindres pleins : Ic = (mr^2/4 + ml^2/12, mr^2/4 + ml^2/12, mr^2/2)
% Par rapport a leur centre de masse
m = jambeMasse;
r = jambeRayon;
l = jambeLongueur;
jambesMomentInertie = MomentInertieCylindrePlein(m,r,l);
jambesMomentInertie = r_y * jambesMomentInertie * transpose(r_y);

% par rapport au centre de masse du patineur

% jambe gauche
dc = centreDeMasseTotalVertical - jambeGaucheCentreMasse;
Ic = jambesMomentInertie;
Id = Ic + m*Tdc(dc);
jambeGaucheId = Id;

%jambe droite 
dc = centreDeMasseTotalVertical - jambeDroiteCentreMasse;
Ic = jambesMomentInertie;
Id = Ic + m*Tdc(dc);
jambeDroiteId = Id;

% Tronc
% Par rapport a son centre de masse
m = troncMasse;
r = troncRayon;
l = troncLongueur;
troncMomentInertie = MomentInertieCylindrePlein(m,r,l);
troncMomentInertie = r_y * troncMomentInertie * transpose(r_y);

%Par rapport au centre de masse total
dc = centreDeMasseTotalVertical - troncCentreMasse;
Ic = troncMomentInertie;
Id = Ic + m*Tdc(dc);
troncId = Id;

% Cou
% Par rapport a son centre de masse
m = couMasse;
r = couRayon;
l = couLongueur;
couMomentInertie = MomentInertieCylindrePlein(m,r,l);
couMomentInertie = r_y * couMomentInertie * transpose(r_y);

%Par rapport au centre de masse total
dc = centreDeMasseTotalVertical - couCentreMasse;
Ic = couMomentInertie;
Id = Ic + m*Tdc(dc);
couId = Id;

% Tete
% par rapport a son centre de masse
m = teteMasse;
r = teteRayon;
teteMomentInertie = MomentInertieSpherePleine(m,r);
teteMomentInertie = r_y * teteMomentInertie * transpose(r_y);

% par rapport au centre de masse total
dc = centreDeMasseTotalVertical - teteCentreMasse;
Ic = teteMomentInertie;
Id = Ic + m*Tdc(dc);
teteId = Id;


% Bras (cylindres pleins : Ic = (mr^2/4 + ml^2/12, mr^2/4 + ml^2/12, mr^2/2)
% Par rapport a leur centre de masse

% Bras droit
m = brasMasse;
r = brasRayon;
l = brasLongueur;
brasMomentInertie = MomentInertieCylindrePlein(m,r,l);
brasMomentInertie = r_y * brasMomentInertie * transpose(r_y);

dc = centreDeMasseTotalVertical - brasDroitCentreMasseVertical;
Ic = brasMomentInertie;
Id = Ic + m*Tdc(dc);
brasDroitId = Id;

% Bras gauche vertical 
dc = centreDeMasseTotalVertical - brasGaucheCentreMasseVertical;
Ic = brasMomentInertie;
Id = Ic + m*Tdc(dc);
brasGaucheVerticalId = Id;

% Bras gauche horizontal
brasMomentInertie = MomentInertieCylindrePleinAxeY(m,r,l);
brasMomentInertie = r_y * brasMomentInertie * transpose(r_y);
Ic = brasMomentInertie;
dc = centreDeMasseTotalHorizontal - brasGaucheCentreMasseHorizontal;
Id = Ic + m*Tdc(dc);
brasGaucheHorizontalId = Id;

%Les moments d'inerties pour les patineurs
patineurVerticalId = jambeGaucheId + jambeDroiteId + troncId + couId + teteId + brasDroitId + brasGaucheVerticalId
patineurHorizontalId = jambeGaucheId + jambeDroiteId + troncId + couId + teteId + brasDroitId + brasGaucheHorizontalId

%% c) Accélération angulaire du patineur initialement au repos
% On applique une force de F = (0, -200, 0)^T N au point r_f(0, r_h, z_f)^T
% avec z_f = L_j + L_t + L_c + r_h
F = [0; -200; 0];
z_f = jambeLongueur + troncLongueur + couLongueur + teteRayon;
r_f = [z_f*sin(theta); teteRayon; z_f*cos(theta)];
w_initial = [0;0;0];
alphaIncline =  AccelerationAngulaire(F,r_f,centreDeMasseTotalVertical,w_initial,patineurVerticalId)

F = [0; -200; 0];
z_f = jambeLongueur + troncLongueur + couLongueur + teteRayon;
r_f = [z_f*sin(theta); teteRayon; z_f*cos(theta)];
w_initial = [0;0;0];
alphaInclineBrasLeve = AccelerationAngulaire(F,r_f,centreDeMasseTotalVertical,w_initial,patineurHorizontalId)

%% d) Accélération angulaire du patineur 
% vitesse angulaire de w = (0, 0, 10)^T rad/s autour de son centre de masse
% 
F = [0; -200; 0];
z_f = jambeLongueur + troncLongueur + couLongueur + teteRayon;
r_f = [z_f*sin(theta); teteRayon; z_f*cos(theta)];
w_initial = 10*[sin(theta);0;cos(theta)];
alphaInclineNonRepos =  AccelerationAngulaire(F,r_f,centreDeMasseTotalVertical,w_initial,patineurVerticalId)

F = [0; -200; 0];
z_f = jambeLongueur + troncLongueur + couLongueur + teteRayon;
r_f = [z_f*sin(theta); teteRayon; z_f*cos(theta)];
w_initial = 10*[sin(theta);0;cos(theta)];
alphaInclineBrasLeveNonRepos = AccelerationAngulaire(F,r_f,centreDeMasseTotalVertical,w_initial,patineurHorizontalId)