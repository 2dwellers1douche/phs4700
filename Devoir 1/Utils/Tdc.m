function [ tdc ] = Tdc( dc )
%TDC Retourne T(dc) a partir de dc (chap 2 slide 68)
if ~isvector(dc)
    error('Input must be a vector')
end
if length(dc) ~= 3
    error('Input must be a vector of lenght 3')
end

dcx = dc(1);
dcy = dc(2);
dcz = dc(3);

tdc = [ (dcy^2 + dcz^2) -dcx*dcy        -dcx*dcz        ;
        -dcy*dcx        (dcx^2 + dcz^2) -dcy*dcz        ;
        -dcz*dcx        -dcz*dcy        (dcx^2 + dcy^2)];

end

