function [ Ic ] = MomentInertieSpherePleine( masse, rayon )
%MomentInertieSpherePleine REtourne le moment d'inertie d'une spehre pleine
%   Detailed explanation goes here

m = masse;
r = rayon;

Ic = [ (2*m*r^2/5)  0           0           ;
       0            (2*m*r^2/5) 0           ;
       0            0           (2*m*r^2/5)];
end

