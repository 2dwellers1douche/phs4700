function [ Ic ] = MomentInertieCylindrePlein( masse, rayon, longueur )
%MomentInertieCylindrePlein Summary of this function goes here
%   Detailed explanation goes here
m = masse;
r = rayon;
l = longueur;

Ic = [(m*(r^2)/4 + m*(l^2)/12)  0                       0           ;
      0                     (m*(r^2)/4 + m*(l^2)/12)    0           ;
      0                     0                       (m*(r^2)/2)]  ;

end

