function [ Ic ] = MomentInertieParrallelepipede( masse, x, y ,z )
%MomentInertieParrallelepipede Retourne le momment d'inertie d'un parrallelepipede x = longueur,
%y = largeur et z = hauteur (chap2 slide 65)
a = x;
b = y;
c = z;
m = masse;

Ic = [(m/12 * (b^2 + c^2))  0                       0                       ;
      0                     (m/12 * (a^2 + c^2))    0                       ;
      0                     0                       (m/12 * (a^2 + b^2))]   ;

end

