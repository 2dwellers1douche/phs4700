function [ Ic ] = MomentInertieCylindrePleinAxeY( masse, rayon, longueur )
%MomentInertieCylindrePleinAxeY Summary of this function goes here
%   Detailed explanation goes here
m = masse;
r = rayon;
l = longueur;

Ic = [(m*(r^2)/4 + m*(l^2)/12)  0                       0           ;
      0                     (m*(r^2)/2)                 0           ;
      0                     0            (m*(r^2)/4 + m*(l^2)/12)]  ;
end