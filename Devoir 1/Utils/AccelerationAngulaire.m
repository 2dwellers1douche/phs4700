function [ alpha ] = AccelerationAngulaire( force, point_force, centreMasse, w_initial, momentInertie )
%ACCELERATIONANGULAIRE Summary of this function goes here
%   Detailed explanation goes here
r = point_force - (centreMasse);
tau = cross(r,force);
L = MomentCinetique(momentInertie, w_initial);
alpha = inv(momentInertie)*(tau + cross(L,w_initial));

end

