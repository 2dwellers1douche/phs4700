function acceleration_balle = Integrande(vitesse_balle, options)

masse_balle = 0.145; %kg
coeff_frottement_visqueux = 1.45;
rho_air = 1.1644; %kg/m^3
vitesse_angulaire = [0; 0; 50]; %rad/s
coefficient_magnus = 1.6*10^(-3) * norm(vitesse_angulaire);
diametre_balle = 0.073; %metre

F_frottement_visqueux = -(pi*(diametre_balle^2)/8)*rho_air* coeff_frottement_visqueux * norm(vitesse_balle) .* vitesse_balle;
F_Magnus = (pi*(diametre_balle^2)/8)*rho_air*coefficient_magnus*norm(vitesse_balle)./norm(vitesse_angulaire)*cross(vitesse_angulaire,vitesse_balle);

if(options == 1)
    F_Magnus = [0; 0; 0];
    F_frottement_visqueux = [0; 0; 0];
end
if(options ==2)
    F_Magnus = [0; 0; 0];
end

F_gravitationnelle = [0; 0; -9.8].* masse_balle;
%a = Ftotale/m
acceleration_balle = (F_frottement_visqueux + F_Magnus + F_gravitationnelle)./masse_balle;
end

