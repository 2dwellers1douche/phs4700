function [prise vf x y z t] = Devoir2(option, vi)

dt = 1*10^(-5); %seconde
%déplacement maximum permis entre deux itération
epsillon = 0.0005; %metre

% position_finale, vitesse_finale, temps_final, nombre_elements_tableau, raison;

marbre_largeur = 0.3048; %metre
hauteur_zone_prise = 1; %metre
hauteur_0_zone_prise = 0.8; %metre

diametre_balle = 0.073; %metre
rayon_balle = diametre_balle / 2; %metre

%positif y par -> la??
%on enleve rappetisse les coins de la zone de prise avec le rayon de la balle pour trouve la zone
%ou la balle est completement à l'intérieur
zone_prise_superieur_gauche = [0; -marbre_largeur/2 + rayon_balle; hauteur_zone_prise + hauteur_0_zone_prise - rayon_balle]; %metre
zone_prise_superieur_droit  = [0;  marbre_largeur/2 - rayon_balle; hauteur_zone_prise + hauteur_0_zone_prise- rayon_balle]; %metre
zone_prise_inferieur_gauche = [0; -marbre_largeur/2 + rayon_balle; hauteur_0_zone_prise + rayon_balle]; %metre
zone_prise_inferieur_droit  = [0;  marbre_largeur/2 - rayon_balle; hauteur_0_zone_prise + rayon_balle]; %metre
xv = [zone_prise_superieur_gauche(2),zone_prise_superieur_droit(2),zone_prise_inferieur_droit(2),zone_prise_inferieur_gauche(2),zone_prise_superieur_gauche(2)];
yv = [zone_prise_superieur_gauche(3),zone_prise_superieur_droit(3),zone_prise_inferieur_droit(3),zone_prise_inferieur_gauche(3),zone_prise_superieur_gauche(3)];




position_balle_initialle = [18.44; 0; 2.1]; %metre

%est precis devient faux si le deplacement entre deux itération
%est plus grand que epsilon
estPrecis = false;

%tant que la simulation n'est pas precices
while(estPrecis == false)
	%initialisation des données de simulations
    t = (0);
    positions_balle = position_balle_initialle;
    vitesses_balle = (vi);
    doitRouler = true;
    prise = 0;
    estPrecis = true;
    doitCheckerPrise = true;
	%tant qu'une condition de sortie n'est pas rencontrée
    while(doitRouler)
		%on trouve la nouvelle vitesse grâce a runge kutta
        nouvelle_vitesse = SEDRK4(vitesses_balle(:,end), dt, 'Integrande', option);
		%avec la vitesse et notre pas de temps, on trouve la nouvelle position
        nouvelle_position = positions_balle(:,end) + nouvelle_vitesse.* dt;
		
		%on trouve lal ongueur du deplacement
        delta_position = positions_balle(:,end) - nouvelle_position;
        magnitude_delta = norm(delta_position);
        
        if(magnitude_delta >= epsillon) %si le déplacement est trop grand
            doitRouler = false;
            dt = dt / 2;
            estPrecis = false;
        else
			%ajout des nouvelles données
            estPrecis = true;
            t = [t t(end)+dt];
            positions_balle = [positions_balle nouvelle_position];
            vitesses_balle = [vitesses_balle nouvelle_vitesse];
            
            
            
            %Tests
            %Testons... le sol!
            if( positions_balle(3,end) <= rayon_balle)
                doitRouler = false;
                prise = 0;
            end
            
            %on regarde si on est par dessus le marbre
            if(doitCheckerPrise && positions_balle(1,end) <= 0)
                %on regarde si on est dans la zone de prise
                xq = (positions_balle(2,end));
                yq = (positions_balle(3,end));
                [in,on] = inpolygon(xq,yq,xv,yv);
                
                %il est important de bien regarder on aussi puisque la bordure est la limite légale
                if(in(1) || on(1))
                    prise = 1;
                    doitRouler = false;
                else
                    prise = 1;
					%on n'a plus besoin de regarder pour des prise
                    doitCheckerPrise = false;
                end
            end
            
        end
    end
end

vf = vitesses_balle(:,end);
x = positions_balle(1,:);
y = positions_balle(2,:);
z = positions_balle(3,:);

end

