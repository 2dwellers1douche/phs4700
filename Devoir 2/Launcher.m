clc
clear all;
close all;

vi1 = [-120;0;4.55];
vi2 = [-120;0;7.79];
vi3 = [-120;1.8;5.63];
vis = [vi1,vi2,vi3]./3.6;
symboles =  ['r.','g+','bo'];

%copie colle de devoir2.m mais cest jsute pour le graphique!
diametre_balle = 0.073; %metre
rayon_balle = diametre_balle / 2; %metre
marbre_largeur = 0.3048; %metre
hauteur_zone_prise = 1; %metre
hauteur_0_zone_prise = 0.8; %metre
%Zone de prise o� la balle est valide
zone_prise_superieur_gauche = [0; -marbre_largeur/2 + rayon_balle; hauteur_zone_prise + hauteur_0_zone_prise - rayon_balle]; %metre
zone_prise_superieur_droit  = [0;  marbre_largeur/2 - rayon_balle; hauteur_zone_prise + hauteur_0_zone_prise- rayon_balle]; %metre
zone_prise_inferieur_gauche = [0; -marbre_largeur/2 + rayon_balle; hauteur_0_zone_prise + rayon_balle]; %metre
zone_prise_inferieur_droit  = [0;  marbre_largeur/2 - rayon_balle; hauteur_0_zone_prise + rayon_balle]; %metre
%Zone totale de prise
zone_prise_superieur_gauche2 = [0; -marbre_largeur/2; hauteur_zone_prise + hauteur_0_zone_prise]; %metre
zone_prise_superieur_droit2  = [0;  marbre_largeur/2; hauteur_zone_prise + hauteur_0_zone_prise]; %metre
zone_prise_inferieur_gauche2 = [0; -marbre_largeur/2; hauteur_0_zone_prise]; %metre
zone_prise_inferieur_droit2  = [0;  marbre_largeur/2; hauteur_0_zone_prise]; %metre
xv = [0,0,0,0,0];
yv = [zone_prise_superieur_gauche(2),zone_prise_superieur_droit(2),zone_prise_inferieur_droit(2),zone_prise_inferieur_gauche(2),zone_prise_superieur_gauche(2)];
zv = [zone_prise_superieur_gauche(3),zone_prise_superieur_droit(3),zone_prise_inferieur_droit(3),zone_prise_inferieur_gauche(3),zone_prise_superieur_gauche(3)];
yv2 = [zone_prise_superieur_gauche2(2),zone_prise_superieur_droit2(2),zone_prise_inferieur_droit2(2),zone_prise_inferieur_gauche2(2),zone_prise_superieur_gauche2(2)];
zv2 = [zone_prise_superieur_gauche2(3),zone_prise_superieur_droit2(3),zone_prise_inferieur_droit2(3),zone_prise_inferieur_gauche2(3),zone_prise_superieur_gauche2(3)];
c = [0.0 0.0 0.0 0.0 0.0];
c2 = [0.4 0.4 0.4 0.4 0.4];
%fin des donn�es requise pour faire notre graphique

%double boucle for pour faire toutes les combinaisons de vitesses et options
for i = 1:3
	%initialisations du graphique
    figure('Name',sprintf('balle %i', i))
    axis equal
    grid on;
    hold on
    for j = 1:3
        [prise vf x y z t] = Devoir2(j,vis(:,i));
        
        sprintf('balle %i option %i', i,j)
        prise
        vf
        pf = [x(end),y(end),z(end)]
        t(end)
            
        plot3(x(1:10:length(x)),y(1:10:length(y)),z(1:10:length(z)),symboles(j))
        
    end
    legend('option 1','option 2', 'option 3')
    fill3(xv,yv2,zv2,c2)
    fill3(xv,yv,zv,c)
    hold off

    
end